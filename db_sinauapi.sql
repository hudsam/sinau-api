-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 08, 2021 at 01:16 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sinauapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `biodata`
--

CREATE TABLE `biodata` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','pending','banned','loss') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biodata`
--

INSERT INTO `biodata` (`id`, `firstname`, `lastname`, `gender`, `status`, `email`, `city`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad', 'Budiman', 'male', 'active', 'muhaman@gmail.com', 'Bogor', 'Psr. Lumban Tobing No. 70', '0818-5007-571', NULL, NULL),
(2, 'Cengkal', 'Megantara', 'female', 'pending', 'cengkkom@gmail.com', 'Kupang', 'Gg. Jakarta No. 623', '0641-7383-1094', NULL, NULL),
(3, 'Winda', 'Wulandari', 'female', 'banned', 'windaarm@gmail.com', 'Pariaman', 'Jr. Sutan Syahrir No. 310', '0402-3632-977', NULL, NULL),
(4, 'KlausRadit', 'Napitupulu', 'male', 'loss', 'raditnulu@gmail.com', 'Palembang', 'Jln. Jambu No. 333', '0927-0558-877', NULL, NULL),
(5, 'Nurul', 'Uyainah', 'female', 'active', 'nuruluah@gmail.com', 'Mojokerto', 'Ds. Ki Hajar Dewantara No. 457', '0305-2342-123', NULL, NULL),
(6, 'Kartika', 'Haryanti', 'female', 'pending', 'kartmpd@gmail.com', 'Tasikmalaya', 'Gg. Setia Budi No. 491', '0900-8304-465', NULL, NULL),
(7, 'Jarwa', 'Tampubolon', 'male', 'banned', 'jarwawlon@gmail.com', 'Pekanbaru', 'Gg. Salatiga No. 294', '022-7178-8150', NULL, NULL),
(8, 'Hasim', 'Situmorang', 'male', 'loss', 'hasimng@gmail.com', 'Tanjung Pinang', 'Ds. Bahagia No. 553', '0715-1025-4275', NULL, NULL),
(9, 'Ratih', 'Rahayu', 'female', 'active', 'ratihyu@gmail.com', 'Blitar', 'Psr. Panjaitan No. 916', '0635-3929-175', NULL, NULL),
(10, 'Nyana', 'Habibi', 'male', 'pending', 'nyanei@gmail.com', 'Cilegon', 'Psr. Gading No. 324', '0583 7237 943', NULL, NULL),
(11, 'Vivi', 'Usada', 'female', 'banned', 'vivida@gmail.com', 'Depok', 'Dk. Bah Jaya No. 508', '0593-1038-476', NULL, NULL),
(12, 'Johan', 'Saefullah', 'male', 'loss', 'johasi@gmail.com', 'Kepulauan Tidore', 'Psr. PHH. Mustofa No. 985', '0331-9704-9823', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_01_08_032103_todo', 1),
(2, '2021_01_08_065119_biodata', 2);

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `aktivitas` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`id`, `aktivitas`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'API', 'Belajar API menggunakan Laravel Lumen', '2021-01-08 03:33:28', '2021-01-08 03:33:28'),
(2, 'Git', 'Belajar Git di GitLab', '2021-01-08 03:40:17', '2021-01-08 03:40:17'),
(4, '-', '-', '2021-01-08 06:48:35', '2021-01-08 06:48:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biodata`
--
ALTER TABLE `biodata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biodata`
--
ALTER TABLE `biodata`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

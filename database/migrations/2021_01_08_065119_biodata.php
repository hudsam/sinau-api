<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Biodata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata', function (Blueprint $table) {
            $table->id();
            $table->string('firstname',50);
            $table->string('lastname', 50);
            $table->enum('gender', array('male', 'female'));
            $table->enum('status', array('active', 'pending', 'banned', 'loss'));
            $table->string('email', 75);
            $table->string('city', 25);
            $table->string('address', 100);
            $table->string('phone', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}

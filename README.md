#### Membuat API menggunakan Lumen - PHP Micro-Framework by Laravel

* End-Point yang tersedia
	* `/select`					: Mengambil semua data yang disimpan
	* `/limit/{number}`			: Mengambil data dengan batasan tertentu
	* `/condition/{string}`		: Mencari data sesuai kebutuhan
	* `/current_page/{param}`	: Mengambil value dari `param`
	* `/order/{string}`			: Mengurutkan data
	* `/add`					: Menambahkan data dengan variabel yang tersedia
	* `/view/{number}`			: Mengambil data dengan `id` tertentu
	* `/update/{number}`		: Mengubah data pada `id` tertentu dengan variabel yang tersedia
	* `/delete/{number}`		: Menghapus data pada `id` tertentu

<br>

Contoh request setiap End-Point
| Method | End-Point				| URl											| Variable(s)			|
|--------|--------------------------|-----------------------------------------------|-----------------------|
| GET	 | `/select`				| ```http://localhost:8000/select```			|						|
| GET	 | `/limit/{number}`		| ```http://localhost:8000/limit/2```			|						|
| GET	 | `/condition/{string}`	| ```http://localhost:8000/condition/male``` 	|						|
| GET	 | `/current_page/{param}`	| ```http://localhost:8000/current_page/12```	|						|
| GET	 | `/order/{string}`		| ```http://localhost:8000/order/desc```		|						|
| POST	 | `/add`					| ```http://localhost:8000/add``` 				| firstname, lastname	|
| GET	 | `/view/{number}`			| ```http://localhost:8000/view/1``` 			|						|
| PUT	 | `/update/{number}`		| ```http://localhost:8000/update/1``` 			| firstname, lastname	|
| DELETE | `/delete/{number}`		| ```http://localhost:8000/delete/1``` 			|						|
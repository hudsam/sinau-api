<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /*
    public function add(Request $request)
    {
        $add = new Todo();
        $add->aktivitas = $request->aktivitas;
        $add->keterangan = $request->keterangan;

        $result = $add->save();
        if ($result)
        {
            $response = [
                'status' => 'berhasil',
                'message' => 'data ditambahkan'
            ];
        }
        else
        {
            $response = [
                'status' => 'gagal',
                'message' => 'data tidak ditambahkan'
            ];
        }
        return response($response);
    }

    public function show()
    {
        $kegiatan = Todo::all();
        $response = [
                'kegiatan' => $kegiatan,
        ];
        return response($response);
    }

    public function detail($id)
    {
        $kegiatan = Todo::where('id', $id)->first();
        if ($kegiatan)
        {
            $response = [
                'status' => 'berhasil',
                'message' => 'data tersedia',
                'kegiatan' => $kegiatan
            ];
        }
        else
        {
            $response = [
                'status' => 'gagal',
                'message' => 'data tidak tersedia',
                'kegiatan' => $kegiatan
            ];
        }

        return response($response);
    }

    public function update(Request $request, $id)
    {
        $kegiatan = Todo::find($id);
        $update = $kegiatan->update([
            'aktivitas' => $request->aktivitas,
            'keterangan' => $request->keterangan
        ]);

        if ($update)
        {
            $response = [
                'status' => 'berhasil',
                'message' => 'data diperbarui'
            ];
        }
        else
        {
            $response = [
                'status' => 'gagal',
                'message' => 'data tidak diperbarui'
            ];
        }
        return response($response);
    }

    public function delete($id)
    {
        $kegiatan = Todo::find($id);
        $remove = $kegiatan->delete();

        if ($remove)
        {
            $response = [
                'status' => 'berhasil',
                'message' => 'data dihapus'
            ];
        }
        else
        {
            $response = [
                'status' => 'gagal',
                'message' => 'data tidak dihapus'
            ];
        }
        return response($response);
    }
    */
    
    //
}

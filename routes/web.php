<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    // return $router->app->version();
    return '<title>hudsam</title>@ Internship Jagoan Hosting Indonesia';
});

$router->get('/select', 'BiodataController@select');

$router->group(['prefix' => 'api/biodata'], function () use ($router)
{
	$router->post('/add', ['uses' => 'BiodataController@add']);
	$router->get('/view/{id}', ['uses' => 'BiodataController@view']);
	$router->put('/update/{id}', ['uses' => 'BiodataController@update']);
	$router->delete('/delete/{id}', ['uses' => 'BiodataController@delete']);
});

$router->get('/limit/{id}', 'BiodataController@limit');
$router->get('/order/{type}', 'BiodataController@order');
$router->get('/current_page/{param}', 'BiodataController@current_page');
$router->get('/condition/{gender}', 'BiodataController@condition');

/*
$router->post('/add', 'TodoController@add');
$router->get('/show', 'TodoController@show');
$router->get('/detail/{id}', 'TodoController@detail');
$router->put('/update/{id}', 'TodoController@update');
$router->delete('/delete/{id}', 'TodoController@delete');

// Generate APP_KEY
$router->get('/key', function() {
	return Str::random(32);
});

$router->get('/get', function() {
	return 'GET';
});

$router->post('/post', function() {
	return 'POST';
});

$router->put('/put', function() {
	return 'PUT';
});

$router->patch('/patch', function() {
	return 'PATCH';
});

$router->delete('/delete', function() {
	return 'DELETE';
});

$router->options('/options', function() {
	return 'OPTIONS';
});

$router->get('/mahasiswa/{nim}', function($nim) {
	return 'NIM: ' . $nim;
});

$router->get('/post/{postId}/comments/{commentId}', function($postId, $commentId) {
	return 'Post: ' . $postId . ' / Comment: ' . $commentId;
});

$router->get('/optional[/{param}]', function($param = null) {
	return 'Param: ' . $param;
});

$router->get('/profile', function() {
	return redirect()->route('profile.hudsam');
});

$router->get('/profile/hudsam', ['as' => 'profile.hudsam', function() {
	return 'Profil: hudsam';
}]);

$router->group(['prefix' => 'user'], function () use ($router) {
	$router->get('auth', function() {
		return 'Halaman auth untuk user';
	});
	$router->get('profile', function() {
		return 'Halaman profile untuk user';
	});
});
*/
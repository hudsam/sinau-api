<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biodata;

class BiodataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Mengambil semua data
    public function select()
    {
        $biodata = Biodata::all();
        $response = [
            'biodata' => $biodata,
        ];
        return response($response);
    }

    // Menambah data
    public function add(Request $request)
    {
        $add = new Biodata();
        $add->firstname = $request->firstname;
        $add->lastname = $request->lastname;

        $result = $add->save();
        if ($result)
        {
            $response = [
                'status' => $result,
                'pesan' => 'Biodata bertambah'
            ];
        }
        else
        {
            $response = [
                'status' => $result,
                'pesan' => 'Biodata tidak ditambahkan'
            ];
        }

        return response($response);
    }

    // Melihat biodata tertentu
    public function view($id)
    {
        $biodata = Biodata::where('id', $id)->get();
        if ($biodata->count() == 1)
        {
            $response = [
                'biodata' => $biodata
            ];
        }
        else
        {
            $response = [
                'biodata' => null
            ];
        }

        return response($response);
    }

    // Mengubah biodata tertentu
    public function update(Request $request, $id)
    {
        $var = Biodata::where('id', $id)->get();
        if ($var->count() == 1)
        {
            $biodata = Biodata::find($id);
            $update = $biodata
                        ->update([
                            'firstname' => $request->firstname,
                            'lastname' => $request->lastname
                        ]);
            if ($update)
            {
                $response = [
                    'status' => true,
                    'pesan' => 'Biodata ' . $id . ' berubah'
                ];
            }
            else
            {
                $response = [
                    'status' => false,
                    'pesan' => 'Biodata ' . $id . ' tidak berubah'
                ];
            }
        }
        else
        {
            $response = [
                'status' => false,
                'pesan' => 'Biodata ' . $id . ' tidak tersedia'
            ];
        }
        return response($response);
    }

    // Menghapus biodata tertentu
    public function delete($id)
    {
        $count = Biodata::where('id', $id)->get()->count();
        if ($count == 1)
        {
            $search = Biodata::find($id);
            $biodata = $search->delete();
            $response = [
                'status' => true,
                'pesan' => 'Biodata ' . $id . ' dihapus'
            ];
        }
        else
        {
            $response = [
                'status' => false,
                'pesan' => 'Biodata ' . $id . ' tidak tersedia'
            ];
        }

        return response($response);
    }

    // Membatasi data yang diambil
    public function limit($id)
    {
        $biodata = Biodata::limit($id)->get();
        if ($id <= 0)
        {
            $response = [
                'pesan' => 'Parameter harus > 1',
                'biodata' => null
            ];
        }
        else
        {
            $response = [
                'pesan' => 'Tersedia ' . $id . ' biodata',
                'biodata' => $biodata
            ];
        }
        return response($response);
    }

    // Mencari data sesuai kebutuhan (gender)
    public function condition($gender)
    {
        $biodata = Biodata::where('gender', strtolower($gender))->get();
        $response = [
            'pesan' => 'Ada ' . count($biodata) . ' biodata dengan jenis ' . strtolower($gender),
            'biodata' => $biodata
        ];
        return response($response);
    }

    // Mengambil value dari variabel param
    public function current_page($param)
    {
        if ($param >= 1)
        {
            $response = [
                'current_page' => $param,
            ];
        }
        else
        {
            $response = [
                'current_page' => null,
            ];
        }
        return response($response);
    }

    // Mengurutkan data
    public function order($type)
    {
        $biodata = Biodata::orderBy('firstname', strtoupper($type))->get();
        $response = [
            'pesan' => 'Biodata diurutkan dengan ' . strtoupper($type),
            'biodata' => $biodata,
        ];
        return response($response);
    }

    //
}
